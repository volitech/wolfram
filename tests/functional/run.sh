#!/bin/bash

EXEC="./wolfram"
RESULT_OK="\e[0;32mOK\e[0;0m"
RESULT_KO="\e[0;31mKO\e[0;0m"
RESULT_TIMED_OUT="\e[0;35mTIMED OUT\e[0;0m"
RESULT_FAILED="\e[0;35mFAILED\e[0;0m"
RESULT_SKIPPED="\e[0;36mSKIPPED\e[0;0m"
TOTAL=0
PASSED=0
FAILED=0
TIMEOUT=15

function cleanup {
    rm -f .out.txt
    rm -f .result.txt
}

function test {
    NAME="$1"
    ARGS="$2"
    EXPECTED_EXIT_CODE="$3"
    OUT_PATH="./tests/functional/outputs/$NAME.txt"

    printf "%-32s" "  Test $1: "
    TOTAL=$(($TOTAL + 1))
    echo "$EXEC $ARGS" | timeout --foreground $TIMEOUT bash - > .out.txt 2> /dev/null
    EXIT_CODE=$?

    if [[ $EXIT_CODE == 124 ]]; then
        printf "$RESULT_TIMED_OUT\n    Timed out!\n"
        cleanup; return
    fi
    if [[ $EXIT_CODE == 130 ]]; then
        printf " - $RESULT_SKIPPED\n    received SIGINT\n"
        TOTAL=$(($TOTAL - 1))
        cleanup; return
    fi
    if [[ $EXIT_CODE != $EXPECTED_EXIT_CODE ]]; then
        printf "$RESULT_KO\n    Expected return code $EXPECTED_EXIT_CODE, got $EXIT_CODE\n"
        cleanup; return
    fi
    if [[ $EXIT_CODE != 0 ]]; then
        printf "$RESULT_OK\n"
        PASSED=$(($PASSED + 1))
        cleanup; return
    fi
    if [[ ! -f $OUT_PATH ]]; then
        printf "$RESULT_FAILED\n    Output file not found\n"
        return
    fi

    diff $OUT_PATH -y .out.txt > .result.txt
    DIFF_RET=$?
    [ "$DIFF_RET" != "0" ] \
        && (printf "$RESULT_KO\n    Invalid output:\n"; \
            cat .result.txt | awk '{print "\t"$0}') \
        || printf "$RESULT_OK\n"
    [ "$DIFF_RET" == "0" ] && PASSED=$(($PASSED + 1))
    cleanup
    return $DIFF_RET
}

if [[ ! -x "$EXEC" || ! -f $EXEC ]]; then
    printf "$RESULT_FAILED: file $EXEC not found or is not executable!\n"
    exit 1
fi

echo "[Basic cases]"
    test "r30_l10" "--rule 30 --lines 10" 0
    test "r110_l16" "--rule 110 --lines 16" 0
    test "r90_l32" "--rule 90 --lines 32" 0
    test "r94_l24" "--rule 94 --lines 24" 0
echo

echo "[Advanced rules]"
    test "r255_l12" "--rule 255 --lines 12" 0
    test "r1_l16" "--rule 1 --lines 16" 0
    test "r253_l18" "--rule 253 --lines 18" 0
    test "r20_l24" "--rule 20 --lines 24" 0
echo

echo "[Advanced options]"
    test "r30_l10_w10_m4" "--rule 30 --lines 10 --window 10 --move 4" 0
    test "r110_l16_w30_m12" "--rule 110 --lines 16 --window 30 --move 12" 0
    test "r20_l24_w30_m24" "--rule 20 --lines 24 --window 30 --move -24" 0
    test "r1_l1_w3_m1" "--rule 1 --lines 1 --window 3 --move 1" 0
echo

echo "[Rigor]"
    test "no_rule" "--window 32 --lines 10" 84
    test "invalid_rule" "--rule 256 --lines 10" 84
    test "invalid_number" "--rule 30 --lines 16 --move 12a" 84
    test "negative_lines" "--rule 90 --lines -4" 84
echo

printf "\nRESULT:\n"
printf "  $PASSED / $TOTAL\n"
printf "  $(printf $(($PASSED * 10000 / $TOTAL)) | sed 's/..$/.&/') %%\n"
if [[ $FAILED != "0" ]]; then
    printf "\nWARNING!!! $FAILED test(s) failed\n"
    exit 1
fi
[ $PASSED != $TOTAL ] && exit 1 || exit 0
