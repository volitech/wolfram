import System.Environment
import System.Exit
import Control.Monad

import Wolfram

dieWithError :: [Char] -> IO ()
dieWithError str = do
    putStrLn $ "Error: " ++ str
    exitWith $ ExitFailure 84

readInt :: [Char] -> Maybe Int
readInt str = case (reads str) :: [(Int, [Char])] of
      [(n, "")] -> Just n
      otherwise -> Nothing

getArgValue :: [[Char]] -> [Char] -> Maybe Int -> Maybe Int
getArgValue [] _ _default = _default
getArgValue [_] _ _default = _default
getArgValue (k:v:next) key _default = if key == k
    then readInt v
    else getArgValue ([v] ++ next) key _default

checkExistance :: [Maybe Int] -> IO ()
checkExistance [] = return ()
checkExistance (n:next) = do
    case n of
        Nothing     -> dieWithError "invalid or unprovided arguments"
        otherwise   -> checkExistance next

checkPositiveness :: [Maybe Int] -> IO ()
checkPositiveness [] = return ()
checkPositiveness (Nothing:next) = checkPositiveness next
checkPositiveness (Just n:next) = if n < 0
    then dieWithError "inappropriate negative value"
    else checkPositiveness next

myIntAbs :: Int -> Int
myIntAbs n = if n < 0
    then (-n)
    else n

unjustify :: Maybe Int -> Int
unjustify (Just n) = n
unjustify Nothing = -1

main = do
    args <- getArgs
    let rule        = getArgValue args "--rule" $ Nothing
    let lines       = getArgValue args "--lines" $ Nothing
    let start       = getArgValue args "--start" $ Just 0
    let width       = getArgValue args "--window" $ Just 80
    let offset      = getArgValue args "--move" $ Just 0
    let line_size   = ceiling $ (fromIntegral $ (unjustify width) - 1) / 2
    let tab_size    = (abs $ unjustify offset) + line_size
    let tab_str     = getTabulationStr tab_size
    checkPositiveness [rule, lines, start, width]
    checkExistance [rule, start, width, offset]
    when ((unjustify rule) > 255) $ dieWithError "rule must be at most 255"
    wolfram
        (tab_str ++ "*" ++ tab_str)
        (unjustify rule)
        ((unjustify lines) + (unjustify start))
        (unjustify start)
        (unjustify width)
        (unjustify offset)
    return ()
