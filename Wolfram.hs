module Wolfram where

import System.Exit
import Data.Bits
import Control.Monad

getChild :: [Char] -> Int -> Char
getChild "   " n = if (.&.) n 1 /= 0 then '*' else ' '
getChild "  *" n = if (.&.) n 2 /= 0 then '*' else ' '
getChild " * " n = if (.&.) n 4 /= 0 then '*' else ' '
getChild " **" n = if (.&.) n 8 /= 0 then '*' else ' '
getChild "*  " n = if (.&.) n 16 /= 0 then '*' else ' '
getChild "* *" n = if (.&.) n 32 /= 0 then '*' else ' '
getChild "** " n = if (.&.) n 64 /= 0 then '*' else ' '
getChild "***" n = if (.&.) n 128 /= 0 then '*' else ' '

getNextRow :: [Char] -> Int -> [Char]
getNextRow (a:b:c:next) rule =
    [getChild ([a]++[b]++[c]) rule] ++ getNextRow ([b] ++ [c] ++ next) rule
getNextRow _ _ = ""

getTabulationStr :: Int -> [Char]
getTabulationStr n = concat $ replicate (max 0 n) " "

trimLine :: [Char] -> Int -> [Char]
trimLine "" _ = ""
trimLine str n = if n < 1
    then str
    else if mod n 2 == 0
        then trimLine (init $ tail str) (n - 2)
        else trimLine (init str) (n - 1)

addLineOffset :: [Char] -> Int -> [Char]
addLineOffset str n = if n < 0
    then str ++ (getTabulationStr $ -n * 2)
    else (getTabulationStr (n * 2)) ++ str

printRow :: [Char] -> Int -> Int -> IO()
printRow str _width _offset = do
    putStrLn $ trimLine line ((length line) - _width)
        where
            tab_size = ceiling $ (fromIntegral $ _width - length str) / 2
            tab = getTabulationStr tab_size
            line = addLineOffset (tab ++ str ++ tab) _offset

wolfram :: [Char] -> Int -> Int -> Int -> Int -> Int -> IO()
wolfram _ _ 0 _ _ _ = return ()
wolfram row rule n skip _width _offset = do
    when (skip <= 0) $ printRow row _width _offset
    wolfram
        (getNextRow ("  " ++ row ++ "  ") rule)
        rule
        (n - 1)
        (skip - 1)
        _width
        _offset
