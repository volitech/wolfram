##
## EPITECH PROJECT, 2020
## Makefile
## File description:
## Makefile for wolfram
##

NAME		=	wolfram
CC			=	ghc

SRC			=	Main.hs

OBJ			=	$(SRC:%.hs=$(OBJ_PATH)/%.o)

OBJ_PATH	=	obj
SRC_PATH	=	src

.SILENT:

all: init $(NAME) finish

init:
	printf "Building $(NAME) on "
	TZ='Europe/Paris' date +"%x, %T"
	echo ""
	mkdir -p $(OBJ_PATH)

finish:
	if [ "$(compiled)" == "" ]; then \
		echo "No scripts compiled."; \
	else \
		printf "\nCompiled "; \
		printf "$(compiled)" | wc -w | tr -d "\n"; echo " script(s)."; \
	fi
	echo "Generated executable \"$(NAME)\"."

$(NAME): clean
	$(CC) $(SRC) -o $(NAME) -odir $(OBJ_PATH) -hidir $(OBJ_PATH)

clean:
	rm -Rf obj/
	rm -Rf $(SRC_PATH)/*.hi
	printf "%-16b Cleaned object files.\n" "[$(NAME)]:"

fclean: clean
	rm -f $(NAME)
	printf "%-16b Removed executable.\n" "[$(NAME)]:"

re:
	make fclean
	echo ""
	make all

.PHONY: all init finish clean fclean re
